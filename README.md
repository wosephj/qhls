# README #

Hold nginx config files I used for testing an HLS live stream feed.
I was able to succesfully view a live webcam feed on desktop Chrome, Firefox, IE, Edge. I was also able to view on Android (Nexus 5) Chrome, and iPhone.

### Tools used: ###
* nginx
* nginx-rtmp-module
* ffmpeg
* videojs 
* videojs-contrib-hls
